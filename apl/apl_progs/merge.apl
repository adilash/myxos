decl
	integer a1,a2,b1,b2,c1,c2,i,j,al,bl,status;
enddecl
integer main()
{
	breakpoint;
	status=Create("a.dat");
	print(status);
	breakpoint;
	status=Open("a.dat");
	print(status);
	breakpoint;
	print("Opened a");
	a1=1;
	a2=100;
	i=a1;
	al=0;
	breakpoint;
	while(i<=a2) do
		if(i%2!=0) then
			status=Write(0,i);
			al=al+1;
			if(status==0) then
				print(i);
			endif;
		endif;
		i=i+1;
	endwhile;
	breakpoint;
	print("Written a");

	status=Create("b.dat");
	print(status);
	breakpoint;
	status=Open("b.dat");
	print(status);
	breakpoint;
	print("Opened b");
	b1=50;
	b2=150;
	i=b1;
	bl=0;
	breakpoint;
	while(i<=b2) do
		if(i%2==0) then
			status=Write(1,i);
			bl=bl+1;
			if(status==0) then
				print(i);
			endif;
		endif;
		i=i+1;
	endwhile;
	breakpoint;
	print("Written b");
	status=Seek(0,0);
	status=Seek(1,0);
	print("Seeked");
	status=Create("c.dat");
	print(status);
	breakpoint;
	status=Open("c.dat");
	print(status);
	breakpoint;
	print("Opened c");
	c1=0;
	c2=0;
	i=-1;
	j=-1;
	while(c1<al && c2<bl) do
		if(i==-1) then
			status=Read(0,i);
		endif;
		if(j==-1) then
			status=Read(1,j);
		endif;
		if(i<j) then
			status=Write(2,i);
			print(i);
			c1=c1+1;
			i=-1;
		else
			status=Write(2,j);
			print(j);
			c2=c2+1;
			j=-1;
		endif;	
	endwhile;
	breakpoint;
	print("Merge1");
	while(c1<al) do
		if(i==-1) then
			status=Read(0,i);
		endif;
		status=Write(2,i);
		print(i);
		i=-1;
		c1=c1+1;
	endwhile;
	while(c2<bl) do
		if(j==-1) then
			status=Read(1,j);
		endif;
		status=Write(2,j);
		print(j);
		j=-1;
		c2=c2+1;
	endwhile;
	status=Close(0);
	status=Close(1);
	status=Close(2);
	//status=Delete("a.dat");
	//status=Delete("b.dat");
	//print("Delete a and b");
	return 0;
}